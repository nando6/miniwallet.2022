<?php
defined('BASEPATH') or exit('No direct script access allowed');

// require_once("./third_party/dompdf/autoload.inc.php");
require_once APPPATH . 'libraries/dompdf/autoload.inc.php';
use Dompdf\Dompdf;

class Pdfgenerator
{

    public function generate($html, $filename = '', $stream = TRUE, $paper = 'A4', $orientation = "portrait")
    {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        
        if ($stream) {
            $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
        } else {
            return $dompdf->output();
        }
    }


    public function create_pdf_qrcode($html, $filename, $stream = TRUE)
    {
        $dompdf = new Dompdf([
            'enable_remote' => true,
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A5', 'portrait');
        $dompdf->render();

        $dir = './assets/generate/pdf_qrcode_tenant/';

        if ($stream) {
            // $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
            file_put_contents($dir.$filename.'.pdf', $dompdf->output());

        } else {
            $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
        }

        return [
            'path' => 'assets/generate/pdf_qrcode_tenant/'.$filename.'.pdf',
            'file' => $filename.'.pdf'
        ];
    }

    
    public function gen_pdf_etiket($html, $filename)
    {
        $dompdf = new Dompdf([
            'enable_remote' => true,
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $dir = './assets/qrcode_konser/user_pdf/';

        // if ($stream) {
            // $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
            file_put_contents($dir.$filename.'.pdf', $dompdf->output());

        // } else {
        //     return $dompdf->output();
        // }

        return [
            'path' => $dir.$filename.'.pdf',
            'file' => $filename.'.pdf'
        ];
    }
}
