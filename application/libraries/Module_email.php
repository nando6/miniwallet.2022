<?php

class Module_email {
    public function send_email($from, $arr_to, $subject, $email_content, $template_file, $attachments = '') {

        $ci =& get_instance();

        $ci->load->library('email');

        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'smtpsmg.gmedia.net.id',
            'smtp_port' => 25,
            'charset' => 'utf-8',
            'wordwrap' => true,
            'mailtype' => 'html'
        ];

        $ci->email->initialize($config);
        $ci->email->clear(true);

        $from_name = isset($from['name']) ? $from['name'] : '';
        $from_email = isset($from['email']) ? $from['email'] : '';
        $to = isset($arr_to['to']) ? $arr_to['to'] : '';
        $cc = isset($arr_to['cc']) ? $arr_to['cc'] : '';
        $bcc = isset($arr_to['bcc']) ? $arr_to['bcc'] : '';

        $ci->email->from($from_email, $from_name);
        $ci->email->to($to);
        $ci->email->cc($cc);
        $ci->email->bcc($bcc);

        $ci->email->subject($subject);

        $body = $ci->load->view($template_file, $email_content, true);

        $ci->email->message($body);

        if (!empty($attachments)) {
            if (is_array($attachments)) {
                $no = 1;

                foreach ($attachments as $attachment) {
                    $custom_name = isset($attachment['name']) ? $attachment['name'] : 'Attachment '.$no++;

                    $ci->email->attach($attachment['location'], 'attachment', $custom_name);
                }

            } else {
                // $custom_name = isset($attachments['name']) ? $attachments['name'] : 'Attachment';
                $ci->email->attach($attachments, 'attachment');

            }
        }

        if($ci->email->send()) {
            return '1';

        } else {
            return $ci->email->print_debugger();
            
        }
    }
}