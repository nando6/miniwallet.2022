<?php

class Module_sms
{
    public function send_sms_old($no_hp = '', $message = '')
    {
        $json = json_encode(array('nomor' => $no_hp, 'pesan' => $message));
        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_URL => "https://erpsmg.gmedia.id/smsgw/api/sms_osbond",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POSTFIELDS =>$json,
                CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function send_sms($no_hp = '', $message = ''){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://sms-gmedia.gmedia.bz/api/send-message',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => array('sender' => 'gmediawablez','number' => $no_hp,'message' => $message),
          CURLOPT_HTTPHEADER => array(
            'token: eyJpdiI6ImhYc3NnOE9xbzZrWXg0bHFmQnZFM1E9PSIsInZhbHVlIjoiQXdEbkcxVk1LSFZKOFpCaHBpdHpGdz09IiwibWFjIjoiYjIzMDFiYzgwMGFlZTAxNWEyNDRhZWJjMTAyMjJkMTNjNzA2ZjVhNWZjMjJmNTQzYTFjYjE1NmZiNTNiYzlmNSJ9'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    public function send_wa($sender = '', $token = '', $no_hp = '', $message = '', $attach = ''){

        $curl = curl_init();

        $url = 'http://sms-gmedia.gmedia.bz/api/send-message';
        $body = array('sender' => $sender,'number' => $no_hp,'message' => $message);

        if($attach != ''){

            $url = 'https://sms-gmedia.gmedia.bz/api/send-message-media';
            $body = array(
              'sender'    => $sender,
              'number'    => $no_hp,
              'caption'   => $message,
              'file'      => $attach);
        }

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => false,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => $body,
          CURLOPT_HTTPHEADER => array(
            'token: '.$token
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $res_curl = json_decode($response,true);
        // echo $res_curl['message'];
        return 'Successfully send message';

    }
}
