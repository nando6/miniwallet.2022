<?php defined('BASEPATH') or exit('no direct scripts are allowed');

class Module_auth
{
    private $auth_key;
    private $client_service;
    private $ci;
    private $my_model;
    public $token;
    public $expired_day;

    function __construct()
    {
        $this->auth_key = 'gmedia_semargress';
        $this->client_service = 'frontend-client';

        $this->ci =& get_instance();

        $this->ci->load->model('api/MyModel');

        $this->token = '';
        $this->expired_day = 1;
        $this->key = $this->ci->config->item('key_enc');
    }

    function check_method($method = 'GET')
    {
        $request_method = $_SERVER['REQUEST_METHOD'];

        if ($request_method == $method) {
            return true;

        } else {
            $json = [
                'response' => '',
                'metadata' => [
                    'status' => 405,
                    'message' => 'Request method not allowed'
                ]
            ];

            $this->print_json(200, $json);

        }
    }

    function check_header()
    {
        $auth_key = $this->ci->input->get_request_header('Auth-Key', true);
        $client_service = $this->ci->input->get_request_header('Client-Service', true);

        if ($auth_key == $this->auth_key && $client_service == $this->client_service) {
            return true;

        } else {
            $json = [
                'response' => '',
                'metadata' => [
                    'status' => 401,
                    'message' => 'Unauthorized, invalid header'
                ]
            ];

            $this->print_json(200, $json);

        }
    }

    function check_token_user()
    {
        $uid = $this->ci->input->get_request_header('Uid', TRUE);
        $token = $this->ci->input->get_request_header('Token', TRUE);

        $q = $this->ci->db->query("
            SELECT *
            FROM users_authentication a
            INNER JOIN tb_user b ON a.users_id = b.uid
            WHERE a.`users_id` = '$uid'
            AND b.status = 1
            AND a.token = '$token'")->row();

        if (!empty($q)) {
            return true;
        } else {
            return false;
        }
    }

    function check_token_merchant()
    {
        $uid = $this->ci->input->get_request_header('Username', TRUE);
        $token = $this->ci->input->get_request_header('Token', TRUE);

        $q = $this->ci->db->query("SELECT *
                FROM users_authentication a
                    INNER JOIN (
                        SELECT m.email AS username
                        FROM merchandise m
                        WHERE m.status = 1
                        UNION
                        SELECT um.username
                        FROM tb_user_merchant um
                        WHERE um.status = 1
                        UNION
                        SELECT v.username
                        FROM tb_volunteer v
                        WHERE v.status = 1
                    ) m1 ON m1.username = a.users_id
                    WHERE a.users_id = '$uid'
                    AND a.token = '$token'
                    AND a.expired_at >= NOW()")->row();

        if (!empty($q)) {
            return true;
        } else {
            return false;
        }
    }

    function check_auth_user($method = 'GET')
    {
        $check_method = $this->check_method($method);

        if ($check_method) {
            if ($this->check_header()) {
                if ($this->check_token_user()) {
                    return true;
                } else {
                    $json = [
                        'response' => '',
                        'metadata' => [
                            'status' => 401,
                            'message' => 'Unauthorized, invalid UID or Token'
                        ]
                    ];

                    $this->print_json(200, $json);
                }
            }
        }
    }

    function check_auth_merchant($method = 'GET')
    {
        $check_method = $this->check_method($method);

        if ($check_method) {
            if ($this->check_header()) {
                if ($this->check_token_merchant()) {
                    return true;
                } else {
                    $json = [
                        'response' => '',
                        'metadata' => [
                            'status' => 401,
                            'message' => 'Unauthorized, invalid Uid or Token'
                        ]
                    ];

                    $this->print_json(200, $json);
                }
            }
        }
    }


    public function set_expired_day($length = 0)
    {
        $this->expired_day = $length;
    }


    public function create_token($uid = '')
    {
        $this->token = crypt(substr(md5(date("Y m d H i s u")), 0, 7), $this->key);
        $expired_at = date("Y-m-d H:i:s", strtotime('+'. $this->expired_day .' days'));

        $data = array(
            'users_id' => $uid,
            'token' => $this->token,
            'expired_at' => $expired_at
        );

        $this->ci->MyModel->process_data('users_authentication', $data);
    }


    public function get_token()
    {
        return $this->token;
    }


    function print_json($status_header, $response)
    {
        $ci =& get_instance();
        $ci->output->set_content_type('application/json');
        $ci->output->set_status_header($status_header);
        $ci->output->set_output(json_encode($response));
    }
}
