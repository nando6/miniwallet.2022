<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Token
{
    protected $ci;
    private $Authorization = 'Token 6b3f7dc70abe8aed3e56658b86fa508b472bf238';

    function __construct()
    {
        $this->ci =& get_instance();
    }

    /*
        GETTING STARTED
        1. Create table users_authentication
        2. Create field users_id -- varchar(60)
        3. Create field token -- text
        4. Create field expired_at -- datetime
    */

    // checking method request of client
    function check_method($method = 'GET')
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        if ($request_method == $method) {
            return true;
        } else {
            return false;
        }
    }

    // checking header
    function check_header()
    {
        $authorization = $this->ci->input->get_request_header('Authorization', true);

        if ($authorization == $this->authorization) {
            return true;
        } else {
            return false;
        }
    }

    // auth
    function auth($method = 'GET', $flag = true)
    {
        // check method request dan method yg ditentukan
        $check_method = $this->check_method($method);
        if ($check_method == true) {
            // check header apakah sudah sesuai
            $check_header = $this->check_header();
            if ($check_header == true) {
                return true;
            } else {
                $response = array(
                    'response' => [],
                    'metadata' => array(
                        'status' => 406,
                        'message' => 'Not acceptable response'
                    )
                );

                return $this->print_json($response);
            }
        } else {
            $response = array(
                'response' => [],
                'metadata' => array(
                    'status' => 405,
                    'message' => 'Method not allowed'
                )
            );

            return $this->print_json($response);
        }
    }

    // fungsi untuk mengeluarkan output json
    function print_json($response = '', $statusHeader = 200)
    {
        $ci =& get_instance();
        $ci->output->set_content_type('application/json');
        $ci->output->set_status_header($statusHeader);
        $ci->output->set_output(json_encode($response));
    }
}

/* End of file Token.php */
/* Location: ./application/libraries/Token.php */
