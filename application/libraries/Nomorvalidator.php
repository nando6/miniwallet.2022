<?php
class Nomorvalidator
{

    // cocokan 3 digit nomor dengan yang didatabase
    function is_valid($nomor = '')
    {
        $ci =& get_instance();
        $provider = $ci->db->where('hlr', $nomor)
                        ->where('status', 1)
                        ->get('ms_provider')
                        ->row();

        $result = (! empty($provider)) ? true : false;

        return $result;
    }

    // merapikan format nomor handphone
    function formatting($nomor = '')
    {
        $format = '';
        if (is_numeric($nomor)) {
            if (substr($nomor, 0, 1) == '6') {
                if (substr($nomor, 1, 1) == '2') {
                    $nomor = str_replace('62', '0', $nomor);
                    if ((strlen($nomor) > 13) || (strlen($nomor) < 10)) {
                        $status = false;
                        $message = 'Jumlah karakter Invalid';
                    } else {
                        $status = true;
                        $message = 'Valid';
                        $format = substr($nomor, 1, 3);
                    }
                } else {
                    $status = false;
                    $message = 'Invalid number';
                }
            } elseif (substr($nomor, 0, 2) == '08') {
                if ((strlen($nomor) > 13) || (strlen($nomor) < 10)) {
                    $status = false;
                    $message = 'Jumlah karakter Invalid';
                } else {
                    $status = true;
                    $message = 'Valid';
                    $format = substr($nomor, 1, 3);
                }
            } elseif (substr($nomor, 0, 1) == '8') {
                $nomor = '0'.$nomor;
                if ((strlen($nomor) > 13) || (strlen($nomor) < 10)) {
                    $status = false;
                    $message = 'Jumlah karakter Invalid';
                } else {
                    $status = true;
                    $message = 'Valid';
                    $format = substr($nomor, 1, 3);
                }
            } else {
                $status = false;
                $message = 'Invalid number';
            }
        } else {
            if (strpos($nomor, '+62')) {
                $nomor = str_replace('+62', '0', $nomor);
                if ((strlen($nomor) > 13) || (strlen($nomor) < 10)) {
                    $status = false;
                    $message = 'Jumlah karakter Invalid';
                } else {
                    $status = true;
                    $message = 'Valid';
                    $format = substr($nomor, 1, 3);
                }
            } else {
                $status = false;
                $message = 'Invalid number';
            }
        }

        $fn_nomor = $nomor;

        if (substr($nomor, 0, 1) != '0') {
            $fn_nomor = '0'.$nomor;
        }

        $result = array(
            'status' => $status,
            'message' => $message,
            'format' => $format,
            'nomor' => $fn_nomor
        );

        return $result;
    }

    // validate nomor
    function validate($nomor = '')
    {
        $format = $this->formatting($nomor);

        $status = isset($format['status']) ? $format['status'] : false;
        $message = isset($format['message']) ? $format['message'] : 'Invalid number';
        $hasil = isset($format['format']) ? $format['format'] : '';
        $nomor = isset($format['nomor']) ? $format['nomor'] : '';
        if ($status == true) {
            $is_valid = $this->is_valid($hasil);
            if ($is_valid == true) {
                $status = true;
            } else {
                $status = false;
                $message = 'Bukan Nomor Telkomsel';
                $nomor = '';
            }
        } else {
            $status = false;
            $nomor = '';
        }

        $result = array(
            'status' => $status,
            'message' => $message,
            'nomor' => $nomor
        );

        return $result;
    }
}
