<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_ip()
{
	$pc_ip = base_url();
    return $pc_ip;
}

function now()
{
	date_default_timezone_set('Asia/Jakarta');
	return date('Y-m-d H:i:s');
}

function today()
{
	date_default_timezone_set('Asia/Jakarta');
	return date('Y-m-d');
}

function authorization()
{
    $ci =& get_instance();
    $authorization = ($ci->input->get_request_header('Authorization', true)) ? $ci->input->get_request_header('Authorization', true) : '';
    return $authorization;
}

function print_json($status = 200, $message = '', $data = [])
{
    $ci =& get_instance();
    $response = response($status, $message, $data);

    return $ci->token->print_json($response);
}

function tgl_indo($tanggal='')
{
	if ($tanggal != '') {
	    $m_create = date_create($tanggal);
	    $m_format = date_format($m_create, 'n');

	    $d_format = date_format($m_create, 'd');
	    $y_format = date_format($m_create, 'Y');

	    switch ($m_format) {
	        case '1':
	            $bulan = 'Januari';
	            break;
	        case '2':
	            $bulan = 'Februari';
	            break;
	        case '3':
	            $bulan = 'Maret';
	            break;
	        case '4':
	            $bulan = 'April';
	            break;
	        case '5':
	            $bulan = 'Mei';
	            break;
	        case '6':
	            $bulan = 'Juni';
	            break;
	        case '7':
	            $bulan = 'Juli';
	            break;
	        case '8':
	            $bulan = 'Agustus';
	            break;
	        case '9':
	            $bulan = 'September';
	            break;
	        case '10':
	            $bulan = 'Oktober';
	            break;
	        case '11':
	            $bulan = 'Nopember';
	            break;
	        case '12':
	            $bulan = 'Desember';
	            break;

	        default:
	            $bulan = '';
	            break;
	    }

	    return $d_format.' '.$bulan.' '.$y_format;
	} else {
		return null;
	}
}

