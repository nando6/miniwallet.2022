<?php
defined('BASEPATH') or exit('No direct script access allowed');


function json_output($statusHeader, $response)
{
	$ci = &get_instance();
	$ci->output->set_content_type('application/json');
	$ci->output->set_status_header($statusHeader);
	$ci->output->set_output(json_encode($response));
}


if (!function_exists('get_body_json')) {
	function get_body_json()
	{
		return json_decode(file_get_contents('php://input'), true);
	}
}


if (!function_exists('response_api')) {
	function response_api($meta_status = 200, $meta_message = '', $data = '')
	{
		$ci = &get_instance();
		$ci->output->set_content_type('application/json');
		$ci->output->set_status_header(200);

		$response_api = [
			'response' => $data,
			'metadata' => [
				'status' => $meta_status,
				'message' => $meta_message
			]
		];
		$ci->output->set_output(json_encode($response_api));
	}
}
