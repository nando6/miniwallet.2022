<?php defined('BASEPATH') or exit('no direct scripts are allowed');

class Initial_account_wallet extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // $this->load->model([
        //     'api/Model_wisata'
        // ]);
    }

	function init_account($customer_xid = ''){
		$authorization = authorization();
		$data_curl = json_encode([
			'customer_xid' => $customer_xid
		]);

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'http://localhost/api/v1/wallet',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_POSTFIELDS => $data_curl,
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json',
		    'authorization: ' . $authorization
		  ),
		));

		$response_curl = curl_exec($curl);

		curl_close($curl);
		$res_curl = json_decode($response_curl,true);

		
	}
	
    public function index()
    {
        $auth = $this->token->auth('POST',true);
        if($auth){
			$response=[];
            $params =get_params();
			$customer_xid = isset($param['customer_xid']) ? $param['customer_xid'] : '';

			if($customer_xid){
				$this->init_account($customer_xid);
			}
			
			print_json($status,$message,$response);
		}
    }
}
